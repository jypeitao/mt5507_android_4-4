package com.mediatek.mmpcm.device;

import com.mediatek.dm.DeviceManagerEvent;

public interface DevListener {
    public void onEvent(DeviceManagerEvent event);
}
