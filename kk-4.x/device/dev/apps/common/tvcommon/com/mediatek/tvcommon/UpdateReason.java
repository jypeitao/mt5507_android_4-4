package com.mediatek.tvcommon;

class UpdateReason {
	public static final int SWAP_CH = 1;
	public static final int INSERT_CH = 2;
	public static final int DELETE_CH = 3;
	public static final int SET_TUNNER = 4;
	public static final int SCAN_COMPLETE = 5;
	public static final int ICH_NOTIFY = 6;
	public static final int CLEAR_CHANNELS = 7;
	public static final int RESET_CH_NAME = 8;
	public static final int RESET_CH_FREQ = 9;
	public static final int RESET_CH_MASK = 10;
	public static final int RESET_CH_NUM = 11;
}
