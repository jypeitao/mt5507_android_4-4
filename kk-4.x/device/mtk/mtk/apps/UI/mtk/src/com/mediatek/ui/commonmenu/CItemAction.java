package com.mediatek.ui.commonmenu;

public interface CItemAction {

	public void onPressLeft();
	public void onPressRigth();
	public void onPressEnter();
}
