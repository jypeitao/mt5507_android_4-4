package com.mediatek.timeshift_pvr.manager;

public class PvrConstant {
    
    public static final int FEATURE_NOT_SUPPORT = 1001;
    public static final int RECORD_FINISH_AND_SAVE= 1002;
    
    public static final int Disk_Disconnect = 1012;
    
    /**
     * 
     */
    public static final int Dissmiss_PVR_BigCtrlbar = 10001;
    public static final int Dissmiss_Tshift_BigCtrlbar = 10003;
    
    public static final int Dissmiss_Info_Bar = 10005;
    public static final int Finish_Timeshift_Activity = 10006;

}
