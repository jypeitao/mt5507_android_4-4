package com.mediatek.ui.cec;

import android.content.Context;
import android.util.AttributeSet;

import com.mediatek.ui.menu.commonview.CustListView;

public class CecListView extends CustListView{

    public CecListView(Context context) {
        super(context);
    }

    public CecListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CecListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

}
