package com.mediatek.timeshift_pvr.ui;

import android.view.View;

public interface OnValueChangedListener {
     public void onValueChanged(View v,int value); 
    
}
