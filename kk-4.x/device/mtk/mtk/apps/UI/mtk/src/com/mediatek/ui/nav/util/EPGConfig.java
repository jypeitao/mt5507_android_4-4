package com.mediatek.ui.nav.util;

public class EPGConfig {
	public static final int mMaxDayNum = 8;
	public static final int mTimeSpan = 2;
	public static final int EPG_SYNCHRONIZATION_MESSAGE = 1;
	 public static final int EPG_PROGRAMINFO_SHOW =2;
	
	public static boolean  init =true;
    public static final int FROM_KEYCODE_DPAD_LEFT =21;
    public static final int FROM_KEYCODE_DPAD_RIGHT =22;
    public static final int FROM_KEYCODE_MTKIR_RED =23;
    public static final int FROM_KEYCODE_MTKIR_GREEN =24;
	public static final int FROM_ANOTHER_STREAM = 25;
	public static final int AVOID_PROGRAM_FOCUS_CHANGE = 26;
    public static final int FROM_KEYCODE_MTKIR_OTHER =-1;
    public static int    SELECTED_CHANNEL_POSITION =0;
    public static  int FROM_WHERE =-1;
    
    public static  final int  EPG_DATA_RETRIEVING =4;
    public static  final int  EPG_DATA_RETRIEVAL_FININSH =5;
    public static final int EPG_EVENT_REASON_SCHEDULE_UPDATE_REPEAT = 6;
    public static boolean avoidFoucsChange = false;
   
}
