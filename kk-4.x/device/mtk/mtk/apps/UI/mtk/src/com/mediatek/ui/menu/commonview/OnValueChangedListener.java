package com.mediatek.ui.menu.commonview;

import android.view.View;

public interface OnValueChangedListener {
     public void onValueChanged(View v,int value); 
    
}
