/*****************************************************************************
 *
 * Filename:
 * ---------
 * gatt_util.h
 *
 * Project:
 * --------
 *   
 *
 * Description:
 * ------------
 *   
 *
 * Author:
 * -------
 * SH Lai
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/

#ifndef __GATT_UTIL_H__
#define __GATT_UTIL_H__

void dumpBtUUID(BT_UUID *bt_uuid);
void dumpAttUUID(ATT_UUID *att_uuid);
void dumpSvcUUID(GATT_SVC_UUID *svc_uuid);

#endif

