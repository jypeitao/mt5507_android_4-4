
# TVOS Res 
PRODUCT_PACKAGES += \
	tvos-framework-res \
        pre_tvos_res

# TCLWidget
PRODUCT_PACKAGES += \
	android.tclwidget \
	android.tclwidget.xml \

#PRODUCT_PACKAGE_OVERLAYS := vendor/tcl/sita/tpackages/UIKit/overlays
#PRODUCT_PACKAGE_OVERLAYS += vendor/tcl/sita/tpackages/UIKit/overlays/tclwidget/6.0
# system
PRODUCT_PACKAGES += \
    com.tcl.os.system

