
*****************************************************************
1.APK名称：          TVGamepad.apk
2.APK英文名称：     
3.APK中文名称：      ATET手柄设置
4.APK包名:           com.atet.tvgamepad
5.APK主入口:            无    
6.厂商定义版本号：3.2.17
7.安装集成说明：
  7.1 集成方法：
   1) 将TVGamepad.apk放置到 system/priv-app ；
   2) 将libatetime.so、libAPKProtect.so库文件放置到 /system/lib中；
   3）请不要修改签名

  7.2 U盘安装方法，如果之前系统已经预置了apk，需到system/priv-app删除原有的apk，以及/data/data目录下的包名，方法如下：
   1).如果是只读文件系统，先remount分区，命令：mount -o remount ,rw /system
   2).删除apk，rm system/priv-app/TVGamepad.apk
   3).删除data数据包， rm -rf /data/data/com.atet.tvgamepad
   4).安装pm install /mnt/usb/XXXX/TVGamepad.apk
   备注：如果不是预置到system/priv-app下的apk，
   可以直接覆盖安装pm install -r /mnt/usb/XXXX/TVGamepad.apk



*****************************************************************
9.版本更改记录：

 9.1 修改记录
   版本提交日期：2014-11-23

   9.1.1 修改问题时间：20141123
   9.1.2 第三方应用版本名称：
         ATET内部版本号：V3.2.17
   9.1.2 修改的问题：
	 1）修改MT5507键值不能用的问题