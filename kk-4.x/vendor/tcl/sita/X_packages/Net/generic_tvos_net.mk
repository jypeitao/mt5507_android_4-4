
#PPPoE
PRODUCT_PACKAGES += \
	pppd \
	pppoe \
	pppoe-stop \
	pppoe-status \
	pppoe-start \
	pppoe-setup \
	pppoe-init \
	pppoe-connect \
	pppoe.conf

#ping add by hqs@tcl
PRODUCT_PACKAGES += \
	tcl_ping


#HuanAccount
PRODUCT_PACKAGES += \
    com.tcl.net.ethernet \
    com.tcl.net.pppoe
