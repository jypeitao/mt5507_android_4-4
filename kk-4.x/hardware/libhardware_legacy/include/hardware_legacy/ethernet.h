#ifndef _ETHERNET_H
#define _ETHERNET_H

#if __cplusplus
extern "C" {
#endif


extern int ethernet_is_driver_loaded();

extern int ethernet_load_driver();


#if __cplusplus
};  // extern "C"
#endif

#endif  // _WIFI_H


