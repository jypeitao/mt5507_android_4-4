/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _ANDROID_KEYCODES_H
#define _ANDROID_KEYCODES_H

/******************************************************************
 *
 * IMPORTANT NOTICE:
 *
 *   This file is part of Android's set of stable system headers
 *   exposed by the Android NDK (Native Development Kit).
 *
 *   Third-party source AND binary code relies on the definitions
 *   here to be FROZEN ON ALL UPCOMING PLATFORM RELEASES.
 *
 *   - DO NOT MODIFY ENUMS (EXCEPT IF YOU ADD NEW 32-BIT VALUES)
 *   - DO NOT MODIFY CONSTANTS OR FUNCTIONAL MACROS
 *   - DO NOT CHANGE THE SIGNATURE OF FUNCTIONS IN ANY WAY
 *   - DO NOT CHANGE THE LAYOUT OR SIZE OF STRUCTURES
 */

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Key codes.
 */
enum {
    AKEYCODE_UNKNOWN         = 0,
    AKEYCODE_SOFT_LEFT       = 1,
    AKEYCODE_SOFT_RIGHT      = 2,
    AKEYCODE_HOME            = 3,
    AKEYCODE_BACK            = 4,
    AKEYCODE_CALL            = 5,
    AKEYCODE_ENDCALL         = 6,
    AKEYCODE_0               = 7,
    AKEYCODE_1               = 8,
    AKEYCODE_2               = 9,
    AKEYCODE_3               = 10,
    AKEYCODE_4               = 11,
    AKEYCODE_5               = 12,
    AKEYCODE_6               = 13,
    AKEYCODE_7               = 14,
    AKEYCODE_8               = 15,
    AKEYCODE_9               = 16,
    AKEYCODE_STAR            = 17,
    AKEYCODE_POUND           = 18,
    AKEYCODE_DPAD_UP         = 19,
    AKEYCODE_DPAD_DOWN       = 20,
    AKEYCODE_DPAD_LEFT       = 21,
    AKEYCODE_DPAD_RIGHT      = 22,
    AKEYCODE_DPAD_CENTER     = 23,
    AKEYCODE_VOLUME_UP       = 24,
    AKEYCODE_VOLUME_DOWN     = 25,
    AKEYCODE_POWER           = 26,
    AKEYCODE_CAMERA          = 27,
    AKEYCODE_CLEAR           = 28,
    AKEYCODE_A               = 29,
    AKEYCODE_B               = 30,
    AKEYCODE_C               = 31,
    AKEYCODE_D               = 32,
    AKEYCODE_E               = 33,
    AKEYCODE_F               = 34,
    AKEYCODE_G               = 35,
    AKEYCODE_H               = 36,
    AKEYCODE_I               = 37,
    AKEYCODE_J               = 38,
    AKEYCODE_K               = 39,
    AKEYCODE_L               = 40,
    AKEYCODE_M               = 41,
    AKEYCODE_N               = 42,
    AKEYCODE_O               = 43,
    AKEYCODE_P               = 44,
    AKEYCODE_Q               = 45,
    AKEYCODE_R               = 46,
    AKEYCODE_S               = 47,
    AKEYCODE_T               = 48,
    AKEYCODE_U               = 49,
    AKEYCODE_V               = 50,
    AKEYCODE_W               = 51,
    AKEYCODE_X               = 52,
    AKEYCODE_Y               = 53,
    AKEYCODE_Z               = 54,
    AKEYCODE_COMMA           = 55,
    AKEYCODE_PERIOD          = 56,
    AKEYCODE_ALT_LEFT        = 57,
    AKEYCODE_ALT_RIGHT       = 58,
    AKEYCODE_SHIFT_LEFT      = 59,
    AKEYCODE_SHIFT_RIGHT     = 60,
    AKEYCODE_TAB             = 61,
    AKEYCODE_SPACE           = 62,
    AKEYCODE_SYM             = 63,
    AKEYCODE_EXPLORER        = 64,
    AKEYCODE_ENVELOPE        = 65,
    AKEYCODE_ENTER           = 66,
    AKEYCODE_DEL             = 67,
    AKEYCODE_GRAVE           = 68,
    AKEYCODE_MINUS           = 69,
    AKEYCODE_EQUALS          = 70,
    AKEYCODE_LEFT_BRACKET    = 71,
    AKEYCODE_RIGHT_BRACKET   = 72,
    AKEYCODE_BACKSLASH       = 73,
    AKEYCODE_SEMICOLON       = 74,
    AKEYCODE_APOSTROPHE      = 75,
    AKEYCODE_SLASH           = 76,
    AKEYCODE_AT              = 77,
    AKEYCODE_NUM             = 78,
    AKEYCODE_HEADSETHOOK     = 79,
    AKEYCODE_FOCUS           = 80,   // *Camera* focus
    AKEYCODE_PLUS            = 81,
    AKEYCODE_MENU            = 82,
    AKEYCODE_NOTIFICATION    = 83,
    AKEYCODE_SEARCH          = 84,
    AKEYCODE_MEDIA_PLAY_PAUSE= 85,
    AKEYCODE_MEDIA_STOP      = 86,
    AKEYCODE_MEDIA_NEXT      = 87,
    AKEYCODE_MEDIA_PREVIOUS  = 88,
    AKEYCODE_MEDIA_REWIND    = 89,
    AKEYCODE_MEDIA_FAST_FORWARD = 90,
    AKEYCODE_MUTE            = 91,
    AKEYCODE_PAGE_UP         = 92,
    AKEYCODE_PAGE_DOWN       = 93,
    AKEYCODE_PICTSYMBOLS     = 94,
    AKEYCODE_SWITCH_CHARSET  = 95,
    AKEYCODE_BUTTON_A        = 96,
    AKEYCODE_BUTTON_B        = 97,
    AKEYCODE_BUTTON_C        = 98,
    AKEYCODE_BUTTON_X        = 99,
    AKEYCODE_BUTTON_Y        = 100,
    AKEYCODE_BUTTON_Z        = 101,
    AKEYCODE_BUTTON_L1       = 102,
    AKEYCODE_BUTTON_R1       = 103,
    AKEYCODE_BUTTON_L2       = 104,
    AKEYCODE_BUTTON_R2       = 105,
    AKEYCODE_BUTTON_THUMBL   = 106,
    AKEYCODE_BUTTON_THUMBR   = 107,
    AKEYCODE_BUTTON_START    = 108,
    AKEYCODE_BUTTON_SELECT   = 109,
    AKEYCODE_BUTTON_MODE     = 110,
    AKEYCODE_ESCAPE          = 111,
    AKEYCODE_FORWARD_DEL     = 112,
    AKEYCODE_CTRL_LEFT       = 113,
    AKEYCODE_CTRL_RIGHT      = 114,
    AKEYCODE_CAPS_LOCK       = 115,
    AKEYCODE_SCROLL_LOCK     = 116,
    AKEYCODE_META_LEFT       = 117,
    AKEYCODE_META_RIGHT      = 118,
    AKEYCODE_FUNCTION        = 119,
    AKEYCODE_SYSRQ           = 120,
    AKEYCODE_BREAK           = 121,
    AKEYCODE_MOVE_HOME       = 122,
    AKEYCODE_MOVE_END        = 123,
    AKEYCODE_INSERT          = 124,
    AKEYCODE_FORWARD         = 125,
    AKEYCODE_MEDIA_PLAY      = 126,
    AKEYCODE_MEDIA_PAUSE     = 127,
    AKEYCODE_MEDIA_CLOSE     = 128,
    AKEYCODE_MEDIA_EJECT     = 129,
    AKEYCODE_MEDIA_RECORD    = 130,
    AKEYCODE_F1              = 131,
    AKEYCODE_F2              = 132,
    AKEYCODE_F3              = 133,
    AKEYCODE_F4              = 134,
    AKEYCODE_F5              = 135,
    AKEYCODE_F6              = 136,
    AKEYCODE_F7              = 137,
    AKEYCODE_F8              = 138,
    AKEYCODE_F9              = 139,
    AKEYCODE_F10             = 140,
    AKEYCODE_F11             = 141,
    AKEYCODE_F12             = 142,
    AKEYCODE_NUM_LOCK        = 143,
    AKEYCODE_NUMPAD_0        = 144,
    AKEYCODE_NUMPAD_1        = 145,
    AKEYCODE_NUMPAD_2        = 146,
    AKEYCODE_NUMPAD_3        = 147,
    AKEYCODE_NUMPAD_4        = 148,
    AKEYCODE_NUMPAD_5        = 149,
    AKEYCODE_NUMPAD_6        = 150,
    AKEYCODE_NUMPAD_7        = 151,
    AKEYCODE_NUMPAD_8        = 152,
    AKEYCODE_NUMPAD_9        = 153,
    AKEYCODE_NUMPAD_DIVIDE   = 154,
    AKEYCODE_NUMPAD_MULTIPLY = 155,
    AKEYCODE_NUMPAD_SUBTRACT = 156,
    AKEYCODE_NUMPAD_ADD      = 157,
    AKEYCODE_NUMPAD_DOT      = 158,
    AKEYCODE_NUMPAD_COMMA    = 159,
    AKEYCODE_NUMPAD_ENTER    = 160,
    AKEYCODE_NUMPAD_EQUALS   = 161,
    AKEYCODE_NUMPAD_LEFT_PAREN = 162,
    AKEYCODE_NUMPAD_RIGHT_PAREN = 163,
    AKEYCODE_VOLUME_MUTE     = 164,
    AKEYCODE_INFO            = 165,
    AKEYCODE_CHANNEL_UP      = 166,
    AKEYCODE_CHANNEL_DOWN    = 167,
    AKEYCODE_ZOOM_IN         = 168,
    AKEYCODE_ZOOM_OUT        = 169,
    AKEYCODE_TV              = 170,
    AKEYCODE_WINDOW          = 171,
    AKEYCODE_GUIDE           = 172,
    AKEYCODE_DVR             = 173,
    AKEYCODE_BOOKMARK        = 174,
    AKEYCODE_CAPTIONS        = 175,
    AKEYCODE_SETTINGS        = 176,
    AKEYCODE_TV_POWER        = 177,
    AKEYCODE_TV_INPUT        = 178,
    AKEYCODE_STB_POWER       = 179,
    AKEYCODE_STB_INPUT       = 180,
    AKEYCODE_AVR_POWER       = 181,
    AKEYCODE_AVR_INPUT       = 182,
    AKEYCODE_PROG_RED        = 183,
    AKEYCODE_PROG_GREEN      = 184,
    AKEYCODE_PROG_YELLOW     = 185,
    AKEYCODE_PROG_BLUE       = 186,
    AKEYCODE_APP_SWITCH      = 187,
    AKEYCODE_BUTTON_1        = 188,
    AKEYCODE_BUTTON_2        = 189,
    AKEYCODE_BUTTON_3        = 190,
    AKEYCODE_BUTTON_4        = 191,
    AKEYCODE_BUTTON_5        = 192,
    AKEYCODE_BUTTON_6        = 193,
    AKEYCODE_BUTTON_7        = 194,
    AKEYCODE_BUTTON_8        = 195,
    AKEYCODE_BUTTON_9        = 196,
    AKEYCODE_BUTTON_10       = 197,
    AKEYCODE_BUTTON_11       = 198,
    AKEYCODE_BUTTON_12       = 199,
    AKEYCODE_BUTTON_13       = 200,
    AKEYCODE_BUTTON_14       = 201,
    AKEYCODE_BUTTON_15       = 202,
    AKEYCODE_BUTTON_16       = 203,
    AKEYCODE_LANGUAGE_SWITCH = 204,
    AKEYCODE_MANNER_MODE     = 205,
    AKEYCODE_3D_MODE         = 206,
    AKEYCODE_CONTACTS        = 207,
    AKEYCODE_CALENDAR        = 208,
    AKEYCODE_MUSIC           = 209,
    AKEYCODE_CALCULATOR      = 210,
    AKEYCODE_ZENKAKU_HANKAKU = 211,
    AKEYCODE_EISU            = 212,
    AKEYCODE_MUHENKAN        = 213,
    AKEYCODE_HENKAN          = 214,
    AKEYCODE_KATAKANA_HIRAGANA = 215,
    AKEYCODE_YEN             = 216,
    AKEYCODE_RO              = 217,
    AKEYCODE_KANA            = 218,
    AKEYCODE_ASSIST          = 219,
    AKEYCODE_BRIGHTNESS_DOWN = 220,
    AKEYCODE_BRIGHTNESS_UP   = 221,
    AKEYCODE_MEDIA_AUDIO_TRACK = 222,
    AKEYCODE_MTKIR_POWER     = 301,
    AKEYCODE_MTKIR_ROOTMENU  = 302,
    AKEYCODE_MTKIR_SOURCE    = 303,
    AKEYCODE_MTKIR_TIMER     = 304,
    AKEYCODE_MTKIR_SLEEP     = 305,
    AKEYCODE_MTKIR_ZOOM      = 306,
    AKEYCODE_MTKIR_PEFFECT   = 307,
    AKEYCODE_MTKIR_SEFFECT   = 308,
    AKEYCODE_MTKIR_ASPECT    = 309,
    AKEYCODE_MTKIR_PIPPOP    = 310,
    AKEYCODE_MTKIR_POPPOS    = 311,
    AKEYCODE_MTKIR_PIPSIZE   = 312,
    AKEYCODE_MTKIR_MTSAUDIO  = 313,
    AKEYCODE_MTKIR_CC        = 314,
    AKEYCODE_MTKIR_SWAP      = 315,
    AKEYCODE_MTKIR_1         = 316,
    AKEYCODE_MTKIR_2         = 317,
    AKEYCODE_MTKIR_3         = 318,
    AKEYCODE_MTKIR_4         = 319,
    AKEYCODE_MTKIR_5         = 320,
    AKEYCODE_MTKIR_6         = 321,
    AKEYCODE_MTKIR_7         = 322,
    AKEYCODE_MTKIR_8         = 323,
    AKEYCODE_MTKIR_9         = 324,
    AKEYCODE_MTKIR_FREEZE    = 325,
    AKEYCODE_MTKIR_0         = 326,
    AKEYCODE_MTKIR_DOT       = 327,
    AKEYCODE_MTKIR_CHUP      = 328,
    AKEYCODE_MTKIR_PRECH     = 329,
    AKEYCODE_MTKIR_VOLUP     = 330,
    AKEYCODE_MTKIR_CHDN      = 331,
    AKEYCODE_MTKIR_MUTE      = 332,
    AKEYCODE_MTKIR_VOLDN     = 333,
    AKEYCODE_MTKIR_MENU      = 334,
    AKEYCODE_MTKIR_UP        = 335,
    AKEYCODE_MTKIR_INFO      = 336,
    AKEYCODE_MTKIR_LEFT      = 337,
    AKEYCODE_MTKIR_ENTER     = 338,
    AKEYCODE_MTKIR_RIGHT     = 339,
    AKEYCODE_MTKIR_GUIDE     = 340,
    AKEYCODE_MTKIR_DOWN      = 341,
    AKEYCODE_MTKIR_EXIT      = 342,
    AKEYCODE_MTKIR_EJECT     = 343,
    AKEYCODE_MTKIR_PLAYPAUSE = 344,
    AKEYCODE_MTKIR_STOP      = 345,
    AKEYCODE_MTKIR_RECORD    = 346,
    AKEYCODE_MTKIR_REWIND    = 347,
    AKEYCODE_MTKIR_FASTFORWARD  = 348,
    AKEYCODE_MTKIR_PREVIOUS     = 349,
    AKEYCODE_MTKIR_NEXT         = 350,
    AKEYCODE_MTKIR_TITLEPBC     = 351,
    AKEYCODE_MTKIR_SUBTITLE     = 352,
    AKEYCODE_MTKIR_REPEAT       = 353,
    AKEYCODE_MTKIR_ANGLE        = 354,
    AKEYCODE_MTKIR_RED          = 355,
    AKEYCODE_MTKIR_GREEN        = 356,
    AKEYCODE_MTKIR_YELLOW       = 357,
    AKEYCODE_MTKIR_BLUE         = 358,
    AKEYCODE_MTKIR_TTX          = 359,
    AKEYCODE_MTKIR_POP          = 360,
    AKEYCODE_MTKIR_TV           = 361,
    AKEYCODE_MTKIR_TV_ANA       = 362,
    AKEYCODE_MTKIR_TV_DIG       = 363,
    AKEYCODE_MTKIR_WIDGET       = 364,
    AKEYCODE_MTKIR_INDEX        = 365,
    AKEYCODE_MTKIR_TOOLS        = 366,
    AKEYCODE_MTKIR_NETFLIX      = 367,
    AKEYCODE_MTKIR_BACKUP_0     = 368,
    AKEYCODE_MTKIR_SURROUND     = 369,
    AKEYCODE_MTKIR_POWER_ON     = 370,
    AKEYCODE_MTKIR_POWER_OFF    = 371,
    AKEYCODE_MTKIR_COMPONENT_1  = 372,
    AKEYCODE_MTKIR_COMPONENT_2  = 373,
    AKEYCODE_MTKIR_PLAY         = 374,
    AKEYCODE_MTKIR_PAUSE        = 375,
    AKEYCODE_MTKIR_POWER_SAVING = 376,
    AKEYCODE_MTKIR_3D           = 377,
    AKEYCODE_MTKIR_FUNCTION_1   = 378,
    AKEYCODE_MTKIR_FUNCTION_2   = 379,
    AKEYCODE_MTKIR_FUNCTION_3   = 380,
    AKEYCODE_MTKIR_FUNCTION_4   = 381,
    AKEYCODE_MTKIR_FUNCTION_5   = 382,
    AKEYCODE_MTKIR_BACKUP_1     = 383,
    AKEYCODE_MTKIR_BACKUP_2     = 384,
    AKEYCODE_MTKIR_BACKUP_3     = 385,
    AKEYCODE_MTKIR_BACKUP_4     = 386,
    AKEYCODE_MTKIR_BACKUP_5     = 387,
    AKEYCODE_MTKIR_BACKUP_6     = 388,
    AKEYCODE_MTKIR_BACKUP_7     = 389,
    AKEYCODE_MTKIR_BACKUP_8     = 390,
    AKEYCODE_MTKIR_BACKUP_9     = 391,
    AKEYCODE_MTKIR_BACKUP_10    = 392,
    AKEYCODE_MTKIR_BACKUP_11    = 393,
    AKEYCODE_MTKIR_BACKUP_12    = 394,
    AKEYCODE_MTKIR_BACKUP_13    = 395,
    AKEYCODE_MTKIR_BACKUP_14    = 396,
    AKEYCODE_MTKIR_BACKUP_15    = 397,
    AKEYCODE_MTKIR_BACKUP_16    = 398,
    AKEYCODE_MTKIR_BACKUP_17    = 399,
    AKEYCODE_MTKIR_CUSTOM_1     = 400,
    AKEYCODE_MTKIR_CUSTOM_2     = 401,
    AKEYCODE_MTKIR_CUSTOM_3     = 402,
    AKEYCODE_MTKIR_CUSTOM_4     = 403,
    AKEYCODE_MTKIR_CUSTOM_5     = 404,
    AKEYCODE_MTKIR_CUSTOM_6     = 405,
    AKEYCODE_MTKIR_CUSTOM_7     = 406,
    AKEYCODE_MTKIR_CUSTOM_8     = 407,
    AKEYCODE_MTKIR_CUSTOM_9     = 408,
    AKEYCODE_MTKIR_CUSTOM_10    = 409,
    AKEYCODE_MTKIR_CUSTOM_11    = 410,
    AKEYCODE_MTKIR_CUSTOM_12    = 411,
    AKEYCODE_MTKIR_CUSTOM_13    = 412,
    AKEYCODE_MTKIR_CUSTOM_14    = 413,
    AKEYCODE_MTKIR_CUSTOM_15    = 414,
    AKEYCODE_MTKIR_CUSTOM_16    = 415,
    AKEYCODE_MTKIR_CUSTOM_17    = 416,
    AKEYCODE_MTKIR_CUSTOM_18    = 417,
    AKEYCODE_MTKIR_CUSTOM_19    = 418,
    AKEYCODE_MTKIR_CUSTOM_20    = 419,
    AKEYCODE_MTKIR_CUSTOM_21    = 420,
    AKEYCODE_MTKIR_CUSTOM_22    = 421,
    AKEYCODE_MTKIR_CUSTOM_23    = 422,
    AKEYCODE_MTKIR_CUSTOM_24    = 423,
    AKEYCODE_MTKIR_CUSTOM_25    = 424,
    AKEYCODE_MTKIR_CUSTOM_26    = 425,
    AKEYCODE_MTKIR_CUSTOM_27    = 426,
    AKEYCODE_MTKIR_CUSTOM_28    = 427,
    AKEYCODE_MTKIR_CUSTOM_29    = 428,
    AKEYCODE_MTKIR_CUSTOM_30    = 429,
    AKEYCODE_MTKIR_CUSTOM_31    = 430,
    AKEYCODE_MTKIR_CUSTOM_32    = 431,
    AKEYCODE_MTKIR_CUSTOM_33    = 432,
    AKEYCODE_MTKIR_CUSTOM_34    = 433,
    AKEYCODE_MTKIR_CUSTOM_35    = 434,
    AKEYCODE_MTKIR_CUSTOM_36    = 435,
    AKEYCODE_MTKIR_CUSTOM_37    = 436,
    AKEYCODE_MTKIR_CUSTOM_38    = 437,
    AKEYCODE_MTKIR_CUSTOM_39    = 438,
    AKEYCODE_MTKIR_CUSTOM_40    = 439,
    AKEYCODE_MTKIR_CUSTOM_41    = 440,
    AKEYCODE_MTKIR_CUSTOM_42    = 441,
    AKEYCODE_MTKIR_CUSTOM_43    = 442,
    AKEYCODE_MTKIR_CUSTOM_44    = 443,
    AKEYCODE_MTKIR_CUSTOM_45    = 444,
    AKEYCODE_MTKIR_CUSTOM_46    = 445,
    AKEYCODE_MTKIR_CUSTOM_47    = 446,
    AKEYCODE_MTKIR_CUSTOM_48    = 447,
    AKEYCODE_MTKIR_CUSTOM_49    = 448,
    AKEYCODE_MTKIR_CUSTOM_50    = 449,
    AKEYCODE_MTKIR_CUSTOM_51    = 450,
    AKEYCODE_MTKIR_CUSTOM_52    = 451,
    AKEYCODE_MTKIR_CUSTOM_53    = 452,
    AKEYCODE_MTKIR_CUSTOM_54    = 453,
    AKEYCODE_MTKIR_CUSTOM_55    = 454,
    AKEYCODE_MTKIR_CUSTOM_56    = 455,
    AKEYCODE_MTKIR_CUSTOM_57    = 456,
    AKEYCODE_MTKIR_CUSTOM_58    = 457,
    AKEYCODE_MTKIR_CUSTOM_59    = 458,
    AKEYCODE_MTKIR_CUSTOM_60    = 459,
    AKEYCODE_MTKIR_CUSTOM_61    = 460,
    AKEYCODE_MTKIR_CUSTOM_62    = 461,
    AKEYCODE_MTKIR_CUSTOM_63    = 462,
    AKEYCODE_MTKIR_CUSTOM_64    = 463,
    AKEYCODE_MTKIR_CUSTOM_65    = 464,
    AKEYCODE_MTKIR_CUSTOM_66    = 465,
    AKEYCODE_MTKIR_CUSTOM_67    = 466,
    AKEYCODE_MTKIR_CUSTOM_68    = 467,
    AKEYCODE_MTKIR_CUSTOM_69    = 468,
    AKEYCODE_MTKIR_CUSTOM_70    = 469,
    AKEYCODE_MTKIR_CUSTOM_71    = 470,
    AKEYCODE_MTKIR_CUSTOM_72    = 471,
    AKEYCODE_MTKIR_CUSTOM_73    = 472,
    AKEYCODE_MTKIR_CUSTOM_74    = 473,
    AKEYCODE_MTKIR_CUSTOM_75    = 474,
    AKEYCODE_MTKIR_CUSTOM_76    = 475,
    AKEYCODE_MTKIR_CUSTOM_77    = 476,
    AKEYCODE_MTKIR_CUSTOM_78    = 477,
    AKEYCODE_MTKIR_CUSTOM_79    = 478,
    AKEYCODE_MTKIR_CUSTOM_80    = 479,
    AKEYCODE_MTKIR_CUSTOM_81    = 480,
    AKEYCODE_MTKIR_CUSTOM_82    = 481,
    AKEYCODE_MTKIR_CUSTOM_83    = 482,
    AKEYCODE_MTKIR_CUSTOM_84    = 483,
    AKEYCODE_MTKIR_CUSTOM_85    = 484,
    AKEYCODE_MTKIR_CUSTOM_86    = 485,
    AKEYCODE_MTKIR_CUSTOM_87    = 486,
    AKEYCODE_MTKIR_CUSTOM_88    = 487,
    AKEYCODE_MTKIR_CUSTOM_89    = 488,
    AKEYCODE_MTKIR_CUSTOM_90    = 489,
    AKEYCODE_MTKIR_CUSTOM_91    = 490,
    AKEYCODE_MTKIR_CUSTOM_92    = 491,
    AKEYCODE_MTKIR_CUSTOM_93    = 492,
    AKEYCODE_MTKIR_CUSTOM_94    = 493,
    AKEYCODE_MTKIR_CUSTOM_95    = 494,
    AKEYCODE_MTKIR_CUSTOM_96    = 495,
    AKEYCODE_MTKIR_CUSTOM_97    = 496,
    AKEYCODE_MTKIR_CUSTOM_98    = 497,
    AKEYCODE_MTKIR_CUSTOM_99    = 498,
    AKEYCODE_MTKIR_CUSTOM_100   = 499,

    AKEYCODE_TCL_MIC			     = 4092
    

    // NOTE: If you add a new keycode here you must also add it to several other files.
    //       Refer to frameworks/base/core/java/android/view/KeyEvent.java for the full list.
};

#ifdef __cplusplus
}
#endif

#endif // _ANDROID_KEYCODES_H
