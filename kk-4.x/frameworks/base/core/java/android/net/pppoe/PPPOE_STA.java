
package android.net.pppoe;

public enum PPPOE_STA
{
    CONNECTED,
    DISCONNECTED,
    CONNECTING
}
